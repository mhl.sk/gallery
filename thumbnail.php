<?php 
	$link = explode("/", $_POST['image']);
	array_map( 'unlink', array_filter((array) glob($link[0] . "/" . $link[1] . "/thumbnail/*") ) );
	if (!file_exists($link[0] . "/" . $link[1] . "/thumbnail")) {
	    mkdir($link[0] . "/" . $link[1] . "/thumbnail", 0777, true);
	}
	copy($_POST['image'], __DIR__ . "/" . $link[0] . "/" . $link[1] . "/thumbnail/" . $link[2]);
?>